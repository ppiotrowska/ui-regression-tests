report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_Backoffice_Login_0_document_0_desktop.png",
        "test": "../bitmaps_test/20210323-151316/backstop_default_Backoffice_Login_0_document_0_desktop.png",
        "selector": "document",
        "fileName": "backstop_default_Backoffice_Login_0_document_0_desktop.png",
        "label": "Backoffice Login",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://admin.stg.devportal.idemia.io/",
        "referenceUrl": "https://admin.pre.devportal.idemia.io/",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "1.22",
          "analysisTime": 82
        },
        "diffImage": "../bitmaps_test/20210323-151316/failed_diff_backstop_default_Backoffice_Login_0_document_0_desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_Backoffice_Login_0_document_1_phone.png",
        "test": "../bitmaps_test/20210323-151316/backstop_default_Backoffice_Login_0_document_1_phone.png",
        "selector": "document",
        "fileName": "backstop_default_Backoffice_Login_0_document_1_phone.png",
        "label": "Backoffice Login",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://admin.stg.devportal.idemia.io/",
        "referenceUrl": "https://admin.pre.devportal.idemia.io/",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "5.30",
          "analysisTime": 34
        },
        "diffImage": "../bitmaps_test/20210323-151316/failed_diff_backstop_default_Backoffice_Login_0_document_1_phone.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_Backoffice_Login_0_document_2_tablet.png",
        "test": "../bitmaps_test/20210323-151316/backstop_default_Backoffice_Login_0_document_2_tablet.png",
        "selector": "document",
        "fileName": "backstop_default_Backoffice_Login_0_document_2_tablet.png",
        "label": "Backoffice Login",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://admin.stg.devportal.idemia.io/",
        "referenceUrl": "https://admin.pre.devportal.idemia.io/",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "1.77",
          "analysisTime": 56
        },
        "diffImage": "../bitmaps_test/20210323-151316/failed_diff_backstop_default_Backoffice_Login_0_document_2_tablet.png"
      },
      "status": "fail"
    }
  ],
  "id": "backstop_default"
});