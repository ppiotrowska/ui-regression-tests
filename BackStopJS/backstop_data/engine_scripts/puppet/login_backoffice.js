module.exports = async (page, scenario, vp) => {
	
  await require('./loadCookies')(page, scenario);
  const puppeteer = require('puppeteer');
	
(async () => {

  const browser = await puppeteer.launch({headless: true, args:['--no-sandbox']});
  const page = await browser.newPage();
  await page.goto('https://admin.pre.devportal.idemia.io/');

	
  // Wait for log in form
  await Promise.all([
    page.waitForSelector('[id="username"]'),
    page.waitForSelector('[id="password"]'),
  ]);

 await page.waitForSelector('[id="menuSignIn"]');
 await page.click('[id="menuSignIn"]');

  // Enter username and password
  await page.type('[id="username"]', 'paulina.piotrowska@idemia.com');
  await page.type('[id="password"]', '!Qazqaz123');

  page.click('[id="loginButton"]');
	
	    // Submit log in credentials and wait for navigation
//  await Promise.all([
//    page.click('button[data-testid="button"]'),
//    page.waitForNavigation({
//      waitUntil: '[id="menuRequests"]',
//    }),
//  ]);
	  
 })();
};